const getHashBmi = () =>
  location.hash.slice(1).split('/')[2];

export default getHashBmi;
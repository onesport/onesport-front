const resolveRoutes = (route) => {
    switch (route) {
        case '/':
            route = '/';
            break;
        case 'bmi':
            route = '/bmi';
            break;
        case 'customer':
            route = '/customer';
            break;
        case 'ecustomer':
            route = '/ecustomer';
            break;
        case 'search':
            route = '/search';
            break;
        case 'historyc':
            route = '/historyc';
            break;
        default:
            route = (!isNaN(route)) ? '/:id' : 'error';
    }
    return route;
};
export default resolveRoutes;
import Header from '../templates/Header';
import Home from '../pages/Home';
import Peso from '../pages/Peso';
import Bmi from '../pages/Bmi';
import Customer from '../pages/Customer';
import ECustomer from '../pages/editCustomer';
import Historyc from '../pages/history';
import Search from '../pages/search';
import Error404 from '../pages/Error404';
import getHash from '../utils/getHash';
import resolveRoutes from '../utils/resolveRoutes';
import { createCustomer } from '../pages/crudCustomers';
import { updateAllCustomer } from '../pages/crudCustomers';

const routes = {
  '/': Home,
  '/:id': Peso,
  '/bmi': Bmi,
  '/customer': Customer,
  '/ecustomer': ECustomer,
  '/search': Search,
  '/historyc': Historyc,
  '/contact': 'Contact',
};
const router = async () => {
  const header = null || document.getElementById('header');
  const content = null || document.getElementById('content');
  const sectionCodigo = null || document.getElementById('sectionCodigo');

  header.innerHTML = await Header();
  let hash = getHash();
  console.log('hash: '+hash);
  let route = await resolveRoutes(hash);
  let render = routes[route] ? routes[route] : Error404;
  content.innerHTML = await render();

  if (hash === 'customer') {
    const btnClick = content.querySelector('#btnSave');
    btnClick.addEventListener('click', async () => {
      const systemId = content.querySelector('#system-id').value;
      const firstname = content.querySelector('#nombre').value;
      const lastname = content.querySelector('#apellido').value;
      const username = 'admin';
      const password = 'admin';
      const height = content.querySelector('#talla').value;
      await createCustomer({ systemId, firstname, lastname, username, password, height });
    })
  }

  if (hash === 'ecustomer') {
    const btnSearch = content.querySelector('#btnUpdate');

    btnSearch.addEventListener('click', async () => {
      const id = content.querySelector('#id').value;
      const systemId = content.querySelector('#system-id').value;
      const firstname = content.querySelector('#nombre').value;
      const lastname = content.querySelector('#apellido').value;
      const username = 'admin';
      const password = 'admin';
      const height = content.querySelector('#talla').value;
      await updateAllCustomer({ id, systemId, firstname, lastname, username, password, height });

    })
  }

  if (hash === 'search') {
    const btnToedit = content.querySelector('#btnToedit');
    btnToedit.addEventListener('click', async () => {
      const codeToedit = content.querySelector('#codetoedit').value;
      location.href = "#/ecustomer/" + codeToedit;
    })
  }

  if (hash === '/') {
    const btnfindCode = null || document.getElementById('findCode');
    if (btnfindCode !== null) {
      btnfindCode.addEventListener('click', async () => {
        const txtCode = content.querySelector('#code').value;
        location.href = "#/" + txtCode;
      })
    }

  }


  if (!isNaN(hash)) {
    const btnCalc = document.getElementById('btnCalc');
    if (btnCalc !== null) {
      btnCalc.addEventListener('click', async () => {
        const txtWeight = content.querySelector('#txtWeight').value;
        location.href = "#/bmi/" + txtWeight;
      })

    }

  }

};

export default router;
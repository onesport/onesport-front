const Header = () => {
    const view = `
      <div class="Header-main">
        <div class="Header-logo">
          <h2>
            <a href="/">
              OneSport
            </a>
          </h2>
        </div>
        <div class="Header-nav">
          <a href="#/about/">
            About
          </a>
        </div>
      </div>
    `;
    return view;
  };
  
  export default Header;
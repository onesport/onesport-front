import getHashBmi from '../utils/getHashBmi';
import { getOneCustomer } from '../pages/crudCustomers';

const editCustomer =  async () => {
    const codeToFind = getHashBmi();    
    const data =  await getOneCustomer(codeToFind);    
    const view = `
    <h2>Edit customer</h2>
  
    <form action="" id="formCustomer">
    <label for="id">
            <span>Codigo</span>
            <input type="text" id="id" value="${data.id}" required>
        </label>
        <label for="system-id">
            <span>Codigo</span>
            <input type="text" id="system-id" value="${data.systemId}" required>
        </label>
        <label for="nombre">
            <span>Nombre</span>
            <input type="text" id="nombre" value="${data.firstname}" required>
        </label>
        <label for="apellido">
            <span>Apellido</span>
            <input type="text" id="apellido" value="${data.lastname}" required>
        </label>
        <label for="talla">
            <span>Talla</span>
            <input type="text" id="talla" value="${data.height}" required>
        </label> 

        <input type="submit" id="btnUpdate"  required>
        
    </form>
    </div>
  `;

  return view;
};
export default editCustomer;
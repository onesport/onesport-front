import { getDataHistory } from "./crudCustomers";
import { getDataBmi } from "./crudCustomers";

const Historyc =  async() => {
  
  const id = localStorage.getItem('id');
  const dataCustomer =  await getDataBmi(id);  
  const dataHistory =  await getDataHistory(id); 
  const view = `
      <div class="Characters-inner">
      <h2>Nombre: ${dataCustomer.firstname} ${dataCustomer.lastname}</h2>          
      <h2>Peso actual: ${dataCustomer.bmi}</h2>
      <table>
      <tr>
          <th>Fecha</th>
          <th>Peso</th>
          <th>IMC</th>          
        </tr>  
      ${dataHistory.map(item => `
      
      <tr>
          <td>${item.dateControl}</td>
          <td>${item.weight}</td>
          <td>${item.bmi}</td>          
        </tr>
      `).join('')}
      <table>
      <button onClick="location.href='#/'">Salir</button>
      </div>
    `;
    return view;
  };
  
  export default Historyc;

  /*
  <h3>Codigo:<span>${data.systemId}</span></h3>         
          <h3>Nombres:<span>${data.firstName} ${data.lastName}</span></h3>
          <h3>Talla:<span>${height}</span></h3>              
  */
import getHash from '../utils/getHash';
import { getOneCustomer } from "./crudCustomers";

const Peso = async () => {
  const idc = getHash();
  const character = await getOneCustomer(idc);

  if (character.length !== 0) {

    localStorage.setItem('id', character.id);
    localStorage.setItem('idc', idc);
    localStorage.setItem('heightls', character.height);
    const view = `
      <div class="Characters-inner">       
        <article class="Characters-card">
        <h3>Codigo: <span>${character.systemId}</span></h3>         
          <h3>Nombres: <span>${character.firstname} ${character.lastname}</span></h3>
          <h3>Talla: <span>${(character.height === null) ? '' : character.height}</span></h3>   
          
          <div id="sectionWeight">  
          <h2>Peso</h2>
          <input type="text" id="txtWeight">        
          </div>
          
          <button id="btnCalc">Calcular</button>
     
      </div>
    `;
    return view;
  } else {
    const view = `
  <div class="Characters-inner">       
  
    <h3>Codigo NO encontrado </h3>         
  
      <button onClick="location.href='#/'">Inicio</button>
      
 
  </div>
`;
    return view;
  }

};
export default Peso;

const Customer =  () => {
    
    const view = `
    <div>
    <form class="formulario" action="" id="formCustomer">
    <div class="formulario__grupo" id="grupo__usuario">
        <label for="system-id">
            <span>Codigo</span>
            <input type="text" id="system-id" required>
        </label>
    </div>
    <div class="formulario__grupo" id="grupo__usuario">
        <label for="nombre" class="formulario__label">
            <span>Nombre</span>
            <input type="text" id="nombre" required class="formulario__input">
        </label>
    </div>

    <div class="formulario__grupo" id="grupo__usuario">
        <label for="apellido">
            <span>Apellido</span>
            <input type="text" id="apellido" required>
        </label>
    </div>
    <div class="formulario__grupo" id="grupo__usuario">
        <label for="talla">
            <span>Talla</span>
            <input type="text" id="talla" required>
        </label>
    </div>
        <input type="submit" id="btnSave"  required>
        
    </form>
    </div>
  `;

  return view;
};
export default Customer;
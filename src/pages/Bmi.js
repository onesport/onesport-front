import getHashBmi from '../utils/getHashBmi';
import { updateCustomer } from "./crudCustomers";
import { addHistory } from "./crudCustomers";
import { getDataBmi } from "./crudCustomers";

const Bmi = async () => {
  const id = localStorage.getItem('id');
  const customerId = id;
  
  const idc = localStorage.getItem('idc');

  const height = localStorage.getItem('heightls');
  const weight = getHashBmi();
  const bmi = weight / (height*height)
  const imgbmi = 'https://pbs.twimg.com/media/EXWRg9LU0AAt_QF.jpg';
  const imgDiet = 'https://i.ibb.co/mh9vw4S/diet1.jpg';
  let dietId;
  let workoutId;
  if(bmi<20){
    dietId = 1;
    workoutId=1;
  }else{
    dietId = 1;
    workoutId=2;
  }
  const fecha= new Date();
  const dateControl = fecha.getFullYear()+'-'+fecha.getMonth()+1+'-'+fecha.getDate();

  await updateCustomer({id,weight ,bmi,dietId,workoutId});  
  await addHistory({dateControl,weight,bmi,customerId});  
  
  const data =  await getDataBmi(id);  
 
    const view = `
      <div class="Characters-inner">       
        <article class="Characters-card">       
       
        <h3>Nombres:  <span>${ data.firstname} ${data.lastname}</span></h3>
        <h3>Talla:  <span>${ data.height}</span></h3> 
        <h3>Peso:  <span>${ data.weight}</span></h3> 
        <hr>
        <div>
        <h2 class="calculado">IMC:  <span>${data.bmi}</span></h2> 
        </div>

        <img src="${imgbmi}"></img>
          <h2><strong>Dieta</strong></h2>          
          <img src="${imgDiet}"></img>
          
          <hr>

          <h2><strong>Rutinas</strong></h2>    
          <h3><span>${data.exercice_one}</span></h3>
                
          <button onClick="location.href='#/historyc'">Ver historial</button>
        </article>

      </div>
    `;

    return view;
  };
  
  export default Bmi;
